import express from "express";
import routerAu from './authrouter';
import routerUs from './userrouter';
import routerZO from './zonerouter';
import routerCT from './colorTyperouter';
import routerSec from "./secteurrouter";
import routerCat from "./categorierouter";
import routerRole from "./rolerouter";
import routerMenu from "./menurouter";
import routerQ from "./questionrouter";
import routerTh from "./themerouter";
import routerAns from "./answersrouter";
import routerNiv from "./niveaurouter";
import routerOrg from "./organisationrouter";
import routerSprint from "./sprintrouter";
import router_questionnaire from "./questionnairerouter";

import router_report from "./reportrouter";

const router = express()


router.use('/user', routerUs);
router.use('/auth', routerAu);
router.use('/zone', routerZO);
router.use('/color_type', routerCT);
router.use('/secteur', routerSec);
router.use('/categorie', routerCat);
router.use('/role', routerRole);
router.use('/menu', routerMenu);
router.use('/question', routerQ);
router.use('/theme', routerTh);
router.use('/answer', routerAns);
router.use('/niveau', routerNiv);
router.use('/organisation', routerOrg);
router.use('/sprint', routerSprint);
router.use('/questionnaire', router_questionnaire);

router.use('/report', router_report);

export default router;